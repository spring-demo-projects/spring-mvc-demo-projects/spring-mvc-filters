package com.cdac.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class LoggingFilter implements Filter {

	public LoggingFilter() {
		System.out.println("Constructor of : " + this.getClass().getName());
	}

	@Override
	public void init(FilterConfig filterConfig) {
		System.out.println("init() method has been get invoked");
		System.out.println("Filter name is " + filterConfig.getFilterName());
		System.out.println("ServletContext name is" + filterConfig.getServletContext());
		System.out.println("init() method is ended");
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		System.out.println("doFilter() method is invoked");
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		filterChain.doFilter(httpServletRequest, httpServletResponse);
		System.out.println("doFilter() method is ended");
	}

	@Override
	public void destroy() {
		System.out.println("destroy() method is invoked");
	}

}
